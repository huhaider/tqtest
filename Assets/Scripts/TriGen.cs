﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class MeshData
{
    public Mesh Mesh { get; set; }
    public List<Vector3> Vertices { get; set; }
    public List<int> Triangles { get; set; }
    public int VerticesOffset { get; set; }



    public MeshData(Mesh mesh)
    {
        Mesh            = mesh;
        Vertices        = new List<Vector3>();
        Triangles       = new List<int>();
        VerticesOffset  = 0;
    }

    public void Update()
    {
        Mesh.Clear();
        Mesh.vertices = Vertices.ToArray();
        Mesh.triangles = Triangles.ToArray();
        Mesh.RecalculateNormals();
        Reset();
    }

    private void Reset()
    {
        // Reset properties
        Vertices.Clear();
        Triangles.Clear();
        VerticesOffset = 0;
    }

}

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class TriGen : MonoBehaviour
{
    [SerializeField]
    float triEdgeLength     = 0f;
    public float TriEdgeLength { get { return triEdgeLength; } }
    [SerializeField]        
    float triBaseLength     = 0f;
    public float TriBaseLength { get { return triBaseLength; } }

    MeshData meshData;
    [SerializeField]
    float triHeight         = 0.3f;
    float triHalfBaseLength = 0f;

    public Vector3 Center { get; set; }

    private MeshCollider meshCollider;
    public MeshCollider MeshCollider { get { return meshCollider; } }

    public void Initialize()
    {
        float edgeRatio = Mathf.Cos(Mathf.Deg2Rad * 45);
        if (triBaseLength == 0f)
        {
            triBaseLength = triEdgeLength / edgeRatio;
        }
        if (triEdgeLength == 0f)
        {
            triEdgeLength = triBaseLength * edgeRatio;
        }
        triHalfBaseLength = triBaseLength / 2;

        Center = new Vector3(triHalfBaseLength, triHeight / 2, triHalfBaseLength / 2);
    }

    void Awake()
    {
        Initialize();
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        meshData = new MeshData(mesh);
    }

    // Start is called before the first frame update
    void Start()
    {
        GenerateDuoTriMesh();
        meshCollider = (MeshCollider) gameObject.AddComponent(typeof(MeshCollider));
        meshCollider.sharedMesh = meshData.Mesh;
        meshCollider.convex = true;
    }

    #region Methods for making Quad Tris

    private void AddQuadTriangle(TriFaceType faceType) 
    {
        switch (faceType) 
        {
            case TriFaceType.Top:
                meshData.Vertices.Add(new Vector3(triHalfBaseLength, triHeight, triHalfBaseLength));
                meshData.Vertices.Add(new Vector3(triBaseLength, triHeight, 0));
                meshData.Vertices.Add(new Vector3(0, triHeight, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.VerticesOffset += 3;
                break;
            case TriFaceType.Bottom:
                meshData.Vertices.Add(new Vector3(triHalfBaseLength, 0, triHalfBaseLength));
                meshData.Vertices.Add(new Vector3(triBaseLength, 0, 0));
                meshData.Vertices.Add(new Vector3(0, 0, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.VerticesOffset += 3;
                break;
        }
    }

    // Creating vertices around quad clockwise from top left
    private void AddQuadSquare(TriFaceType faceType)
    {
        switch (faceType)
        {
            case TriFaceType.Left:
                meshData.Vertices.Add(new Vector3(triBaseLength,     triHeight,                 0));
                meshData.Vertices.Add(new Vector3(triHalfBaseLength, triHeight, triHalfBaseLength));
                meshData.Vertices.Add(new Vector3(triHalfBaseLength,         0, triHalfBaseLength));
                meshData.Vertices.Add(new Vector3(triBaseLength,             0,                 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 3);
                meshData.VerticesOffset += 4;
                break;
            case TriFaceType.Right:
                meshData.Vertices.Add(new Vector3(triHalfBaseLength, triHeight, triHalfBaseLength));
                meshData.Vertices.Add(new Vector3(0,                triHeight,                 0));
                meshData.Vertices.Add(new Vector3(0,             0,                 0));
                meshData.Vertices.Add(new Vector3(triHalfBaseLength,         0, triHalfBaseLength));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 3);
                meshData.VerticesOffset += 4;
                break;
            case TriFaceType.Back:
                meshData.Vertices.Add(new Vector3(0,     triHeight,                 0));
                meshData.Vertices.Add(new Vector3(triBaseLength, triHeight, 0));
                meshData.Vertices.Add(new Vector3(triBaseLength, 0, 0));
                meshData.Vertices.Add(new Vector3(0, 0, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 3);
                meshData.VerticesOffset += 4;
                break;
        }
    }

    private void GenerateQuadTriMesh()
    {
        // Looking at a pizza slice
        AddQuadTriangle(TriFaceType.Top);
        AddQuadSquare(TriFaceType.Left);
        AddQuadSquare(TriFaceType.Right);
        AddQuadSquare(TriFaceType.Back);
        AddQuadTriangle(TriFaceType.Bottom);
        meshData.Update();
    }

    #endregion


    private void AddDuoTriangle(TriFaceType faceType)
    {
        switch (faceType)
        {
            case TriFaceType.Top:
                meshData.Vertices.Add(new Vector3(0, triHeight, 0));
                meshData.Vertices.Add(new Vector3(0, triHeight, triEdgeLength));
                meshData.Vertices.Add(new Vector3(triEdgeLength, triHeight, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.VerticesOffset += 3;
                break;
            case TriFaceType.Bottom:
                meshData.Vertices.Add(new Vector3(0, 0, 0));
                meshData.Vertices.Add(new Vector3(0, 0, triEdgeLength));
                meshData.Vertices.Add(new Vector3(triEdgeLength, 0, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.VerticesOffset += 3;
                break;
        }
    }

    // Creating vertices around quad clockwise from top left
    private void AddDuoSquare(TriFaceType faceType)
    {
        switch (faceType)
        {
            case TriFaceType.Left:
                meshData.Vertices.Add(new Vector3(0, triHeight, triEdgeLength));
                meshData.Vertices.Add(new Vector3(0, triHeight, 0));
                meshData.Vertices.Add(new Vector3(0, 0, 0));
                meshData.Vertices.Add(new Vector3(0, 0, triEdgeLength));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 3);
                meshData.VerticesOffset += 4;
                break;
            case TriFaceType.Right:
                meshData.Vertices.Add(new Vector3(0, triHeight, 0));
                meshData.Vertices.Add(new Vector3(triEdgeLength, triHeight, 0));
                meshData.Vertices.Add(new Vector3(triEdgeLength, 0, 0));
                meshData.Vertices.Add(new Vector3(0, 0, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 3);
                meshData.VerticesOffset += 4;
                break;
            case TriFaceType.Back:
                meshData.Vertices.Add(new Vector3(triEdgeLength, triHeight, 0));
                meshData.Vertices.Add(new Vector3(0, triHeight, triEdgeLength));
                meshData.Vertices.Add(new Vector3(0, 0, triEdgeLength));
                meshData.Vertices.Add(new Vector3(triEdgeLength, 0, 0));
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 0);
                meshData.Triangles.Add(meshData.VerticesOffset + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + 3);
                meshData.VerticesOffset += 4;
                break;
        }
    }

    private void GenerateDuoTriMesh()
    {
        // Looking at a pizza slice
        AddDuoTriangle(TriFaceType.Top);
        AddDuoSquare(TriFaceType.Left);
        AddDuoSquare(TriFaceType.Right);
        AddDuoSquare(TriFaceType.Back);
        AddDuoTriangle(TriFaceType.Bottom);
        meshData.Update();
    }



}
