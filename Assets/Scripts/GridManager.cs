﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField]
    private int gridX;

    [SerializeField]
    private int gridZ;

    [SerializeField]
    private GameObject triPrefab;
    public GameObject TriPrefab { get { return triPrefab; } }

    private float cellWidth;

    private List<TriBehaviour> triList = new List<TriBehaviour>();
    public List<TriBehaviour> TriList { get { return triList; } }

    private List<TriBehaviour> selectedTriList = new List<TriBehaviour>();

    private TriBehaviour selectedTri;

    // Start is called before the first frame update
    void Start()
    {
        TriGen triGen = triPrefab.GetComponent<TriGen>();
        triGen.Initialize();
        cellWidth = triGen.TriEdgeLength;
        for (int gridXCtr = 0; gridXCtr < gridX; gridXCtr++)
        {
            for (int gridZCtr = 0; gridZCtr < gridZ; gridZCtr++)
            {
                Vector3 startLocation = transform.position +
                        transform.forward * cellWidth * gridZCtr +
                        transform.right * cellWidth * gridXCtr;
                int randomDuoNo = UnityEngine.Random.Range(0, 2);
                GenerateDuo(startLocation, (DuoType) randomDuoNo);
            }
        }
        StartCoroutine(CheckForQuadInGrid());
    }

    void GenerateDuo(Vector3 startLocation, DuoType duoType) 
    {
        GameObject gO;
        switch (duoType) 
        {
            case DuoType.Backslash:
                gO = Instantiate(triPrefab,
                                startLocation,
                                transform.rotation,
                                transform);
                SetupTriGo(gO, TriType.One);
                gO = Instantiate(triPrefab,
                                startLocation + transform.forward * cellWidth + transform.right * cellWidth,
                                transform.rotation * Quaternion.Euler(0, 180, 0),
                                transform);
                SetupTriGo(gO, TriType.Two);
                break;
            case DuoType.Forwardslash:
                gO = Instantiate(triPrefab,
                                startLocation + transform.forward * cellWidth,
                                transform.rotation * Quaternion.Euler(0, 90, 0),
                                transform);
                SetupTriGo(gO, TriType.One);
                gO = Instantiate(triPrefab,
                                startLocation + transform.right * cellWidth,
                                transform.rotation * Quaternion.Euler(0, -90, 0),
                                transform);
                SetupTriGo(gO, TriType.Two);
                break;
        }
    }

    void GenerateQuad(Vector3 startLocation)
    {
        GameObject gO;
        gO = Instantiate(triPrefab,
                        startLocation,
                        transform.rotation,
                        transform);
        SetupTriGo(gO);
        gO = Instantiate(triPrefab,
                        startLocation + transform.forward * cellWidth,
                        transform.rotation * Quaternion.Euler(0, 90, 0),
                        transform);
        SetupTriGo(gO);
        gO = Instantiate(triPrefab,
                        startLocation + transform.forward * cellWidth + transform.right * cellWidth,
                        transform.rotation * Quaternion.Euler(0, 180, 0),
                        transform);
        SetupTriGo(gO);
        gO = Instantiate(triPrefab,
                        startLocation + transform.right * cellWidth,
                        transform.rotation * Quaternion.Euler(0, -90, 0),
                        transform);
        SetupTriGo(gO);
    }

    void SetupTriGo(GameObject gO, TriType type = TriType.None)
    {
        TriBehaviour triBehaviour;
        gO.tag = "Tri";
        triBehaviour = gO.GetComponent<TriBehaviour>();
        if (type == TriType.None)
        {
            triBehaviour.SetRandomType();
        }
        else
        {
            triBehaviour.Type = type;
        }
        triBehaviour.DestroyCalled += TriBehaviour_DestroyCalled;
        TriList.Add(triBehaviour);
    }

    private IEnumerator CheckForBrokenQuads()
    {
        yield return new WaitForFixedUpdate();
        List<TriBehaviour> markedAsQuad = TriList.Where(o => o.IsPartOfQuad).ToList();
        foreach (TriBehaviour triB in markedAsQuad)
        {
            triB.CheckForQuad();
        }
    }

    private IEnumerator CheckForQuadInSelectedList()
    {
        yield return new WaitForFixedUpdate();
        foreach (TriBehaviour triB in selectedTriList)
        {
            triB.CheckForQuad();
        }
    }

    private IEnumerator CheckForQuadInGrid() 
    {
        yield return new WaitForFixedUpdate();
        foreach (TriBehaviour triB in TriList)
        {
            triB.CheckForQuad();
        }
    }

    private void DestroyQuads() 
    {
        foreach (TriBehaviour tri in TriList)
        {
            if (tri.IsPartOfQuad)
            {
                tri.DeathByQuad();
            }
        }
    }

    /// <summary>
    /// Remove all trace of the quaded tris    
    /// </summary>
    private void TriBehaviour_DestroyCalled(object sender, EventArgs e)
    {
        TriBehaviour senderTri = (TriBehaviour) sender;
        senderTri.DestroyCalled -= TriBehaviour_DestroyCalled;
        selectedTriList.Remove(senderTri);
        TriList.Remove(senderTri);
    }

    #region Interaction

    private TriBehaviour ScanTriFromInput(Vector2 mousePos) 
    {
        TriBehaviour result = null; 
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(mousePos), out hitInfo);
        if (hit && hitInfo.transform.gameObject.tag == "Tri")
        {
            result = hitInfo.transform.gameObject.GetComponent<TriBehaviour>();
        }
        return result;
    }

    public void Destroy(Vector2 mousePos)
    {
        TriBehaviour selectedTri = ScanTriFromInput(mousePos);
        if(selectedTri == null) { return; }
        if (selectedTri.IsPartOfQuad == true)
        {
            selectedTri.DeathByQuad();
            return;
        }
    }

    public void UpdateSelection(Vector2 mousePos, Vector2 inputDelta)
    {
        TriList.ForEach(o => {o.SelectedState = TriSelectState.Deselected; o.UpdateColor();});
        selectedTri = ScanTriFromInput(mousePos);
        if (selectedTri == null) { return; }
        selectedTri.SelectedState = TriSelectState.Selected;
        selectedTri.UpdateColor();
        selectedTriList = selectedTri.SelectLine(inputDelta);
    }

    public void Deselect() 
    {
        TriList.ForEach(o => { o.SelectedState = TriSelectState.Deselected; o.UpdateColor(); });
        selectedTri = null;
    }

    public void Move(Vector2 inputDelta)
    {
        foreach (TriBehaviour triGO in selectedTriList)
        {
            triGO.Movement.MoveOneCell(inputDelta);
        }
        StartCoroutine(CheckForQuadInSelectedList());
        StartCoroutine(CheckForBrokenQuads());
    }

    #endregion

}
