﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;


public class Controls : MonoBehaviour
{
    private GridManager gridGen;

    private bool useTouch;

    private Vector2 inputStartPos ;
    private Vector2 inputCurrPos ;
    private bool inputStarted;
    [SerializeField]
    private float dragTimeActivation;
    private float dragTimeCurrent;

    public void Start()
    {
        gridGen = GameObject.Find("GridGen").GetComponent<GridManager>();
        useTouch = SystemInfo.deviceType == DeviceType.Handheld ? true : false ;
        Debug.Log("Using touch: " + useTouch);
        ResetInputValues();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateInput();
    }

    private void ResetInputValues() 
    {
        inputStartPos       = Vector2.zero;
        inputCurrPos        = Vector2.zero;
        inputStarted        = false;
        dragTimeCurrent     = 0f;
    }

    private Vector2 GetInputDelta() 
    {
        float moveX = inputCurrPos.x - inputStartPos.x;
        float moveY = inputCurrPos.y - inputStartPos.y;
        Vector2 moveVector = new Vector2(moveX, moveY).normalized;
        return moveVector;
    }

    private void UpdateInput()
    {
        if (GetPressDown()) 
        {
            if (!inputStarted)
            {
                inputStartPos = GetPressPos();
                inputStarted = true;
            }
            inputCurrPos = GetPressPos();
            dragTimeCurrent += Time.deltaTime;
            gridGen.UpdateSelection(inputStartPos, GetInputDelta());
        }
        else
        {
            if (GetInputDelta().magnitude > 0.1 && dragTimeCurrent > dragTimeActivation) 
            {
                gridGen.Move(GetInputDelta());
            }
            else
            {
                gridGen.Destroy(inputCurrPos);
            }
            gridGen.Deselect();
            ResetInputValues();           
        }
    }

    #region Input abstraction

    private bool GetPressDown() 
    {
        bool result = false;
        if (useTouch) 
        {
            result = Input.touchCount > 0 && Input.touches[0].phase != TouchPhase.Ended;
        }
        else
        {
            result = Input.GetMouseButton(0);
        }
        return result;
    }

    private Vector2 GetPressPos() 
    {
        Vector2 result = Vector2.zero;
        if (useTouch)
        {
            if (Input.touchCount > 0)
            {
                result = Input.touches[0].position;
            }
        }
        else
        {
            result = Input.mousePosition;
        }
        return result;
    }

    #endregion


}
