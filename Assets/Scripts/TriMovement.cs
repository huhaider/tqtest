﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class TriMovement : MonoBehaviour
{

    private TriBehaviour triBehaviour;

    // Start is called before the first frame update
    void Start()
    {
        triBehaviour = GetComponent<TriBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void MoveOneCell(Vector2 moveVector) 
    {
        float cellWidth = 2.0f;
        if(triBehaviour.SelectedState == TriSelectState.Forwardslash) 
        {
            // x axis
            if(moveVector.x < 0) 
            {
                transform.Translate(Vector3.left * cellWidth, Space.World);
            }
            else if (moveVector.x > 0 )
            {
                transform.Translate(Vector3.right * cellWidth, Space.World);
            }
        }
        if (triBehaviour.SelectedState == TriSelectState.Backslash)
        {
            // y axis
            if (moveVector.y < 0)
            {
                transform.Translate(Vector3.back * cellWidth, Space.World);
            }
            else if (moveVector.y > 0)
            {
                transform.Translate(Vector3.forward * cellWidth, Space.World);
            }
        }
    }

    public void Rotate() 
    {
        transform.Rotate(0, 90, 0);
    }

}
