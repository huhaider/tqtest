﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum TriFaceType
{
    Top = 0,
    Left = 1,
    Right = 2,
    Back = 3,
    Bottom = 4,
}

