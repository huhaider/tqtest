﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Looking top down, square on at a quad
/// </summary>
public enum QuadTriDirection
{
    Forward = 0,
    Left = 1,
    Right = 2,
    Back = 3,
}

