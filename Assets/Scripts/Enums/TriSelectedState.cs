﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Rows select state, relates to a grid, designed to be looped through
/// </summary>
public enum TriSelectState
{
    Deselected = 0,
    Backslash = 1,
    Forwardslash = 2,
    Selected = 3
}
