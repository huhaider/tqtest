﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Collections;
using UnityEngine;

public class GridLines : MonoBehaviour
{
    [SerializeField]
    private int GridXCount;

    [SerializeField]
    private int GridZCount;

    [SerializeField]
    private int gridY;

    private GameObject lineGo;

    private float lineLength;

    private float gridCellSpacing = 1f;

    [SerializeField]
    private Material lineMaterial;

    [SerializeField]
    private Color GridLineColor;

    private List<GameObject> GridGameObjects = new List<GameObject>();

    private GridManager gridManager;

    [SerializeField]
    private GridType gridType;

    [SerializeField]
    private float lineWidth;

    [SerializeField]
    private Vector2 offsetInMultiplesOfGridSpacing;

    private void Awake()
    {
        gridManager     = gameObject.GetComponentInParent<GridManager>();
        switch (gridType) 
        {
            case GridType.Tri:
                gridCellSpacing = gridManager.TriPrefab.GetComponent<TriGen>().TriEdgeLength;
                break;
            case GridType.Quad:
                gridCellSpacing = gridManager.TriPrefab.GetComponent<TriGen>().TriBaseLength;
                break;
        }
        lineGo          = new GameObject();
        lineLength      = (GridXCount > GridZCount ? GridXCount : GridZCount) * gridCellSpacing;
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateGrid();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatesLinePoints();
    }

    void CreateGrid() 
    {
        Vector3 gridOrgin = transform.position + 
                            transform.right * offsetInMultiplesOfGridSpacing.x * gridCellSpacing + 
                            transform.forward * offsetInMultiplesOfGridSpacing.y * gridCellSpacing;

        for (int gridXCtr = 0; gridXCtr <= GridXCount; gridXCtr++)
        {
            Vector3 startPoint =    gridOrgin +
                                    transform.right * gridXCtr * gridCellSpacing +
                                    transform.up * gridY;
            CreateLine(startPoint, "X", gridXCtr);
        }
        for (int gridZCtr = 0; gridZCtr <= GridZCount; gridZCtr++)
        {
            Vector3 startPoint =    gridOrgin +
                                    transform.forward * gridZCtr * gridCellSpacing +
                                    transform.up * gridY;
            CreateLine(startPoint, "Z", gridZCtr);
        }
    }

    void CreateLine(Vector3 startPoint, string dirStr, int lineNo) 
    {
        Vector3 direction = Vector3.zero;
        switch (dirStr) 
        {
            case "X":
                direction = transform.forward;
                break;
            case "Z":
                direction = transform.right;
                break;
        }
        GameObject gO = Instantiate(lineGo,
                                    startPoint,
                                    Quaternion.LookRotation(direction), 
                                    transform);
        gO.name = "Grid" + dirStr + lineNo;
        // Set up the line
        LineRenderer goLineRenderer = (LineRenderer)gO.AddComponent(typeof(LineRenderer));
        goLineRenderer.startWidth   = lineWidth;
        goLineRenderer.endWidth     = lineWidth;
        goLineRenderer.material     = lineMaterial;
        goLineRenderer.startColor   = GridLineColor;
        goLineRenderer.endColor     = GridLineColor;
        GridGameObjects.Add(gO);
    }

    void  UpdatesLinePoints()
    {
        foreach( GameObject gO in GridGameObjects) 
        {
            LineRenderer goLineRenderer = gO.GetComponent<LineRenderer>();
            Vector3[] points = new Vector3[2];
            points[0] = gO.transform.position;
            points[1] = gO.transform.position + 
                        (gO.transform.rotation * Vector3.forward) * lineLength;
            goLineRenderer.positionCount = 2;
            goLineRenderer.SetPositions(points);
        }
    }
}
