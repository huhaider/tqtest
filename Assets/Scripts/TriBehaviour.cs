﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;

public class TriBehaviour : MonoBehaviour
{
    #region Properties

    private Material triMat;

    [SerializeField]
    private Color color1;

    [SerializeField]
    private Color color2;

    [SerializeField]
    private Color color1Highlight;

    [SerializeField]
    private Color color2Highlight;

    [SerializeField]
    private Color color1Quad;

    [SerializeField]
    private Color color2Quad;

    public TriMovement Movement { get; private set; }

    public TriGen TriGen { get; private set; }

    public bool IsPartOfQuad { get; private set; }

    public TriType Type { get; set; } = TriType.None;

    public TriSelectState SelectedState { get; set; } = TriSelectState.Deselected;

    public event System.EventHandler<EventArgs> DestroyCalled;

    protected Dictionary<QuadTriDirection, TriBehaviour> LinkedQuadTriDict = new Dictionary<QuadTriDirection, TriBehaviour>();

    #endregion

    private void Awake()
    {
        TriGen = GetComponent<TriGen>();
        Movement = GetComponent<TriMovement>();
        triMat = GetComponent<Renderer>().material;
        triMat.color = color1;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnDestroy()
    {
        DestroyCalled?.Invoke(this, EventArgs.Empty);
    }

    public void FlipColor() 
    {
        if(triMat.color == color1)
        {
            triMat.color = color2;
        }
        else
        {
            triMat.color = color1;
        }
    }

    public void SetRandomType() 
    {
        int random = UnityEngine.Random.Range(0,2);
        switch (random) 
        { 
            case 0:
                Type = TriType.One;
                break;
            case 1:
                Type = TriType.Two;
                break;
        }
        UpdateColor();
    }

    private List<TriBehaviour> ScanGrid(TriSelectState selectedState) 
    {
        Vector3 localCenter     = GetComponent<TriGen>().Center;
        Vector3 rayStartPoint   = new Vector3();
        Vector3 rayDirection    = new Vector3();
        switch (selectedState)
        {
            case TriSelectState.Deselected:
                return new List<TriBehaviour>();
            case TriSelectState.Backslash:
                rayStartPoint = transform.TransformPoint(localCenter) + Vector3.forward * 50;
                rayDirection = Vector3.back;
                break;
            case TriSelectState.Forwardslash:
                rayStartPoint = transform.TransformPoint(localCenter) + Vector3.left * 50;
                rayDirection = Vector3.right;
                break;
        }
        RaycastHit[] hitArray = Physics.RaycastAll(rayStartPoint, rayDirection, Mathf.Infinity);
        // Debug.DrawRay(rayStartPoint, rayDirection * 100, Color.red, 30);
        // Does the ray intersect any objects excluding the player layer
        List<TriBehaviour> result = new List<TriBehaviour>();
        if (hitArray.Length == 0) { return result; }
        foreach ( RaycastHit hit in hitArray) 
        {
            if(hit.transform.tag == "Tri") 
            {
                result.Add(hit.transform.gameObject.GetComponent<TriBehaviour>());
            }
        }
        return result;
    }

    public static void UpdateHighlight(List<TriBehaviour> triList, TriSelectState selectedState)
    {
        foreach(TriBehaviour triBehaviour in triList) 
        {
            triBehaviour.SelectedState = selectedState;
            triBehaviour.UpdateColor();
        }
    }

    public void UpdateColor()
    {
        switch (Type)
        {
            case TriType.One:
                triMat.color = IsPartOfQuad ? color1Quad :
                               SelectedState != TriSelectState.Deselected ? color1Highlight : color1;
                break;
            case TriType.Two:
                triMat.color = IsPartOfQuad ? color2Quad :
                               SelectedState != TriSelectState.Deselected ? color2Highlight : color2;
                break;
        }
    }

    /// <summary>
    /// Making sure any changes to Tris are reverted after scan
    /// </summary>
    /// <param name="quadList"></param>
    private void CheckForQuadCleanUp(Dictionary<QuadTriDirection, TriBehaviour> quadList)
    {
        foreach(TriBehaviour triB in quadList.Values) 
        {
            triB.TriGen.MeshCollider.enabled = true;
        }
    }

    private bool ScanQuadTri(Dictionary<QuadTriDirection, TriBehaviour> quadTriList, QuadTriDirection direction)
    {
        float rayLength = 0.5f;
        Vector3 localCenter = GetComponent<TriGen>().Center;
        Vector3 rayStartPoint = transform.TransformPoint(localCenter);
        Vector3 rayDirection = new Vector3();

        switch (direction) 
        {
            case QuadTriDirection.Left:
                rayDirection = transform.TransformDirection(Vector3.left);
                break;
            case QuadTriDirection.Right:
                rayDirection = transform.TransformDirection(Vector3.right);
                break;
            case QuadTriDirection.Forward:
                rayDirection = transform.TransformDirection(Vector3.forward);
                break;
        }

        RaycastHit hit;
        bool hitResult = Physics.Raycast(rayStartPoint, rayDirection, out hit, rayLength);
        // Debug.DrawRay(rayStartPoint, rayDirection * 0.5f, Color.red, 30);
        if (!hitResult) 
        {
            // Everything in list is not part of quad
            quadTriList.Values.ToList().ForEach(o => {  o.IsPartOfQuad = false; 
                                                        o.UpdateColor(); });
            CheckForQuadCleanUp(quadTriList); 
            return false; 
        }
        else 
        {
            quadTriList.Add(direction, hit.transform.gameObject.GetComponent<TriBehaviour>());
            quadTriList[direction].TriGen.MeshCollider.enabled = false;
            return true;
        }
    }

    public bool CheckForQuad()
    {
        LinkedQuadTriDict  = new Dictionary<QuadTriDirection,TriBehaviour>();
        // Back
        LinkedQuadTriDict.Add(QuadTriDirection.Back, this);
        LinkedQuadTriDict[QuadTriDirection.Back].TriGen.MeshCollider.enabled = false;
        if (!ScanQuadTri(LinkedQuadTriDict, QuadTriDirection.Left)) { return false; }
        if (!ScanQuadTri(LinkedQuadTriDict, QuadTriDirection.Right)) { return false; }
        if (!ScanQuadTri(LinkedQuadTriDict, QuadTriDirection.Forward)) { return false; }
        // Quad check
        bool allAreOfSameType = true;
        foreach(TriType type in Enum.GetValues(typeof(TriType)))
        {
            allAreOfSameType = true;
            foreach(TriBehaviour triB in LinkedQuadTriDict.Values) 
            {
                allAreOfSameType &= triB.Type == type;
            }
            // Need to break here to avoid overwriting a positive result
            if (allAreOfSameType) { break; }
        }
        if (allAreOfSameType) 
        {
            LinkedQuadTriDict.Values.ToList().ForEach(o => {  o.IsPartOfQuad = true;           
                                                        o.UpdateColor(); });
            CheckForQuadCleanUp(LinkedQuadTriDict);
            return true;
        }
        else
        {
            LinkedQuadTriDict.Values.ToList().ForEach(o => {  o.IsPartOfQuad = false;           
                                                        o.UpdateColor(); });
            CheckForQuadCleanUp(LinkedQuadTriDict);
            return false;
        }
    }

    protected void FlipAdjacentTriColor()
    {
        // Scan behind
        float rayLength = 0.5f;
        Vector3 localCenter = GetComponent<TriGen>().Center;
        Vector3 rayStartPoint = transform.TransformPoint(localCenter);
        Vector3 rayDirection = new Vector3();
        rayDirection = transform.TransformDirection(Vector3.back);
        RaycastHit hit;
        bool hitResult = Physics.Raycast(rayStartPoint, rayDirection, out hit, rayLength);
        if (hitResult) 
        {
            // Flip color if opposite
            TriBehaviour triB = hit.transform.gameObject.GetComponent<TriBehaviour>();
            if(triB.Type != TriType.None && triB.Type != Type) 
            {
                triB.Type = Type;
                triB.UpdateColor();
                if (triB.CheckForQuad())
                {
                    Debug.Log("Quad by flip");
                    triB.DeathByQuad();
                }
            }
        }
    }

    public void DeathByQuad()
    {
        LinkedQuadTriDict.Values.ToList().ForEach(o => o.FlipAdjacentTriColor());
        LinkedQuadTriDict.Values.ToList().ForEach(o => Destroy(o.gameObject));
    }

    #region Interaction

    public List<TriBehaviour> SelectLine(Vector2 inputDelta)
    {
        List<TriBehaviour> result = new List<TriBehaviour>();

        float something = Mathf.Abs(inputDelta.x) - Mathf.Abs(inputDelta.y);

        if(Mathf.Abs(something) > 0.3f) 
        {
            if(something < 0) 
            {
                result = ScanGrid(TriSelectState.Backslash);
                UpdateHighlight(result, TriSelectState.Backslash);
            }
            else if (something > 0) 
            {
                result = ScanGrid(TriSelectState.Forwardslash);
                UpdateHighlight(result, TriSelectState.Forwardslash);
            }
        }
        return result;
    }


    #endregion
}
